# CHANGELOG.md

## Unreleased

- Drop Python 3.6 and 3.7

## 1.0.0

## 0.2.0

New features:

- Add `task_options` to `execute_graph`.

## 0.1.4

Bug fixes:

- Remove deprecated ewoks_jsonload_hook

## 0.1.3

Bug fixes:

- support pip 24.1 and avoid pyyaml 6.0.2rc1

## 0.1.2

Changes:

- remove deprecated ewokscore Task properties

## 0.1.1

Changes:

- use new "engine" argument instead of the deprecated "binding"

## 0.1.0

New features:

- Execute ewoks graph with dask
