# ewoksdask

*ewoksdask* provides distributed task scheduling for [ewoks](https://ewoks.readthedocs.io/) workflows.

## Install

```bash
pip install ewoksdask[test]
```

## Test

```bash
pytest --pyargs ewoksdask.tests
```

## Documentation

https://ewoksdask.readthedocs.io/

